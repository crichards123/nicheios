//
//  main.m
//  Niche
//
//  Created by Colby Melvin on 6/29/15.
//  Copyright (c) 2015 WaveRider, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMQAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CMQAppDelegate class]));
    }
}
